# -*- coding: utf-8 -*-
"""A python module created from the Module 3 code.

An example of the input file: ../Datasets/AiBi.ubyte.pkl.xz
"""

import lzma
import math
import pickle
import tensorflow as tf

from MLClassUtilities import get_batch

# **************************************************

class Classifier(object):
    """
    The classifier from module 3 packaged as a Python class.

    To override the loss function and optimizer, subclass this class
    and override the loss_fn and classifier_fn functions.

    Parameters
    ----------
    n_classes : int, optional, default=True
        Number of possible outputs
    sess : Tensorflow Session object, optional, default=default session
       Tensorflow session to use. If not specified, the default session created
       will be used (must already exist)

    Examples
    --------
    >>> cls = Classifier()
    Created session
    >>> cls.load_data('../../ARC_NN_Course_Data/AiBi.ubyte.pkl.xz')
    >>> cls.build_model()
    >>> cls.train_it(n_epochs=2)
    Running epoch: 1
    Running epoch: 2
    >>> assert (cls.accuracy > .5)
    """

    # **********************************************

    def __init__(self,
                 n_classes=2,
                 sess=tf.get_default_session()):

        self.n_classes = n_classes

        if sess is None:
            self.sess = tf.Session()
            print("Created session")
        else:
            self.sess = sess

    # **************************************************

    @property
    def accuracy(self):
        """Property to run the test dataset and return the accuracy
        """

        tx, ty, _ = next(get_batch(self.test,
                                   self.test[0].shape[0],
                                   1,
                                   n_classes=self.n_classes))
        return self.sess.run(self._accuracy, feed_dict={self.x: tx,
                                                        self.y: ty})

    # **************************************************

    @staticmethod
    def default_layers(width, n_classes):

        l1_width = 1000
        l2_width = 1000
        l3_width = 1000

        return [(l1_width, tf.sigmoid, 4.0 * math.sqrt(6.0 / (width + l1_width))),
                (l2_width, tf.nn.relu, math.sqrt(2.0 / l1_width)),
                (l3_width, tf.nn.relu, math.sqrt(2.0 / l2_width)),
                (n_classes, None, math.sqrt(2.0 / l3_width))]

    def build_model(self,
                    layers=None):
        """Build the model in Tensorflow.

        Override the loss_fn and optimizer_fn methods in a subclass to
        change either.

        Parameters
        ----------
        layers : list of tuples
           Each item in the list will generate a hidden layer. Each list item is a 3-tuple containing
           - The width of the layer
           - The activation function to used
           - The initialization to use for the layers weights
        """

        if layers is None:
            layers = self.default_layers(self.width, self.n_classes)

        self.x = tf.placeholder(tf.float32, [None, self.width])
        self.y = tf.placeholder(tf.float32, [None, self.n_classes])

        prev_width = self.width
        curr_y = self.x

        for i, (layer_width, activation_fn, init_factor) in enumerate(layers):
            with tf.name_scope("layer_{}".format(i)):

                W = tf.Variable(tf.random_uniform([prev_width, layer_width],
                                                  -init_factor, init_factor),
                                name='W_{}'.format(i))
                b = tf.Variable(tf.zeros(layer_width),
                                name='b_{}'.format(i))

                curr_y = tf.nn.xw_plus_b(curr_y, W, b,
                                         name="mn_b_{}".format(i))

                if activation_fn:
                    curr_y = activation_fn(curr_y)

            prev_width = layer_width

        # Call the loss function to build the graph for the loss function

        scalar_loss = self.loss_fn(self.y, curr_y)

        # Call the optimizer function to build the optimization graph

        self.train_step = self.optimizer_fn(scalar_loss, 0.1)

        correct_prediction = tf.equal(tf.argmax(curr_y, 1), tf.argmax(self.y, 1))

        self._accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

    # **************************************************

    def load_data(self, input_file):
        """Load test, training and validation datasets.

        Input file is a Python pickle format file compressed with XZ.

        Parameters
        ----------
        input_file : string
            Name of the input file
        """

        with lzma.open(input_file, "rb") as f:
            self.train = pickle.load(f)
            self.valid = pickle.load(f)
            self.test = pickle.load(f)

        self.width = self.train[0].shape[1]

    # **************************************************

    def loss_fn(self, y, y_prime):
        """Build the graph nodes for the loss function

        Compute the loss for the mini-batch using cross-entropy.

        Override this function is a subclass to replace the
        loss function.

        Parameters
        ----------
        y : tensor
            Labels for the data
        y_prime : tensor
            Estimated labels

        Return
        ------
        Graph node for the scalar loss
        """

        tensor_loss = tf.nn.softmax_cross_entropy_with_logits_v2(labels=y,
                                                                 logits=y_prime)
        return tf.reduce_mean(tensor_loss)

    # **************************************************

    def optimizer_fn(self, loss, learning_rate):
        """Build the graph nodes for the optimizer

        Standard gradient descent optimizer.

        Override this function is a subclass to replace the optimizer.

        Parameters
        ----------
        loss : scalar
            Scalar loss for the mini-batch
        learning_rate : scalar
            You gress

        Return
        ------
        Graph node for the optimizer
        """

        return tf.train.GradientDescentOptimizer(learning_rate).minimize(loss)

    # **************************************************

    def train_it(self,
                 n_epochs=10,
                 batch_size=50):
        """Train the model (duh)

        Parameters
        ----------
        n_epochs : int, optional, default=10
           Number of training epochs
        batch_size : int, optional, default=50
           Mini-batch size
        """

        self.sess.run(tf.global_variables_initializer())

        prev_epoch = -1

        for x_batch, y_batch, epoch in get_batch(self.train,
                                                 batch_size,
                                                 n_epochs,
                                                 n_classes=self.n_classes):

            if prev_epoch != epoch:
                prev_epoch = epoch
                print("Running epoch:", epoch)

            self.sess.run(self.train_step, feed_dict={self.x: x_batch,
                                                      self.y: y_batch})

    # **************************************************

    def validate(self):

        tx, ty, _ = next(get_batch(self.valid,
                                   self.valid[0].shape[0],
                                   1,
                                   n_classes=self.n_classes))

        return self.sess.run(self._accuracy, feed_dict={self.x: tx,
                                                        self.y: ty})


# **************************************************

if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True, report=True)
