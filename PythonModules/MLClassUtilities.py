# -*- coding: utf-8 -*-
"""A python module containing various utilities used in the class
"""

import numpy as np
import tensorflow as tf

# **************************************************

def get_batch(dataset, batch_size, epochs, n_classes=2):
    """Generator function to return mini-batches from the dataset passed.

    Parameters
    ----------
    dataset : tuple
        Tuple containing lists of x and y values.
    batch_size : int
        Mini-batch size
    epochs : int
        Number of training epochs. The entire data will be returned for each epoch.
    n_classes : int, optional, default=2
       Number of unique output classes.

    Yields
    ------
    numpy array [batch_size, x_dimensions]
        Mini-batch of instances scaled from [0,255] to [0.0, 1.0)
    numpy_array [batch_size, n_classes]
        Array of one hot values indicating the class of the instance
    int
        Current epoch number
    """

    epoch = 0
    images, labels = dataset
    n_images = images.shape[0]
    pos = n_images
    s_range = np.arange(n_images)

    while True:

        if pos + batch_size > n_images:
            epoch += 1
            if epoch > epochs:
                break

            i_out = images[pos:]
            l_out = labels[pos:]

            np.random.shuffle(s_range)
            images = images[s_range]
            labels = labels[s_range]

            pos = pos + batch_size - n_images

            i_out = np.concatenate((i_out, images[:pos]))
            l_out = np.concatenate((l_out, labels[:pos]))
        else:
            i_out = images[pos:pos + batch_size]
            l_out = labels[pos:pos + batch_size]
            pos += batch_size

        l_one_hot = np.zeros((batch_size, n_classes))
        l_one_hot[np.arange(batch_size), l_out] = 1.0

        yield i_out / 256, l_one_hot, epoch

# **************************************************

def show_all_variables():
    """Display all current tensorflow variables

    Parameters
    ----------
    None

    Returns
    -------
    Nothing
    """

    for v in tf.global_variables():
        print(v.name, v.shape, v.dtype)


# **************************************************

if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=False, report=True)
