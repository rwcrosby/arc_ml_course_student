# Analytics Research Centre (ARC)
# Machine Learning with Tensorflow &#174;

* Ralph Crosby (ralph.crosby@navy.mil)
* Nicholas Orletsky (nicholas.orletsky@navy.mil)

# Directory Structure

## Module1-7

The course is divided into seven modules. Each module contains the presentation or Jupyter notebooks required for the lession.

## Ancillary

Directory contains various helper files such as graphics.

## Prerequisites

This directory contains Jupyter notebooks to verify that the student has a functional system.

## PythonModules

Various Python tools and samples

# Experimental Datasets

The datasets are contained in a separate git repository: `ARC_NN_COURSE_Data`.

For each dataset there is a Jupyter workbook, an xz compressed tar file of the original images and an xz compressed Python picke file containing the processed images.

Three datasets are used in the class:

1. Aircraft/BIrds
   - AiBi

1. Armored Vehicles/Beaches/Oceans/Warships
   - ArBeOcWa

1. Automobiles/Ships/Trains
   - AuShTr

# Student Versions

For the labs there is both an instructor, master, version of the lab notebooks and a student version that has been stripped down to just contain the questions.
