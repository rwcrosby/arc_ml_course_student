from PIL import Image
import os
import sys

###### Modify the Output Image dimensions and toggle grayscale here ###########
xdim = 150
ydim = 150
#grayscale = True
#output = "Grey"
grayscale = False
output = "RGB"
###############################################################################

dirArray = ['Aircraft', 'Birds']

for i, dir in enumerate(dirArray):
    for f in os.listdir('./' + dir):
        try:
            im = Image.open('./'+ dir + '/' + f)

            if im.height > im.width:
                newHeight = ydim
                newWidth = int(im.width * ydim / im.height)
            else:
                newWidth = xdim
                newHeight = int(im.height * xdim / im.width)

            if grayscale:
                im = im.convert('L')

            # print("From", im.width, im.height, "to", newWidth, newHeight)

            tmp = "./{}{}/{}".format(dir, output, f)
            im.resize((newWidth, newHeight)).save(tmp)

        except (OSError, ValueError) as e:
            print("Error", e, "on", f)
